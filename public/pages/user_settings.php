<?php
#TODO: forgot password/email confirmation
User::logged_in_redirect();
$username = '';

if(!empty($_POST)) {
    /* Clean username and encrypt the password */
    $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    $password = User::encrypt_password($username, $password);


    /* Check for any errors */
    if(empty($_POST['username']) || empty($_POST['password'])) {
        $_SESSION['danger'][] = 'Please fill in all of the fields!';
    }elseif(User::x_exists('username', $username) == false) {
        $_SESSION['danger'][] = 'Invalid Login! Email and Password do not match.';
    }
    elseif(User::login($username, $password) == false) {
        $_SESSION['danger'][] = 'Invalid Login! Email and Password do not match.';
    }



    if(!empty($_POST) && empty($_SESSION['danger'])) {
        $_SESSION['user_id'] = User::login($username, $password);

        $_SESSION['info'][] = 'Welcome back ' . $username;
        redirect('/user-plots');
    }

    display_notifications();

}

initiate_html_columns();

?>


<div class="col-md-2 col-lg-3"></div>
<div class="col-md-8 col-lg-6 boxed">
    <h3 class="title">Login</h3>

    <form action="" method="post" role="form">

        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                </div>
                <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $username; ?>" />
            </div>
        </div>

        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-lock"></i></div>
                </div>
                <input type="password" name="password" class="form-control" placeholder="Password" />
            </div>
        </div>

        <div class="form-group text-center">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Log In</button>
        </div>
        <hr>

        <div class="form-group text-center">
            <a class="btn btn-success" href="<?php echo $settings->url; ?>/register" role="button">Create Account</a>
        </div>




    </form>

</div>


