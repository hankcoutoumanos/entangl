<?php
User::check_permission(0);
//TODO: amount numbers, radius/scale selection
$lon = '';
$lat = '';

if(!empty($_POST)) {
    /* Define some variables */
    $lat = filter_var($_POST['lat'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $lon = filter_var($_POST['lon'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $intent = filter_var($_POST['intent'], FILTER_SANITIZE_STRING);
    $date = time();
    $id = substr(md5(uniqid(rand(), true)), -12);
    $r_lat = json_encode(random_nums(256));
    $r_lon = json_encode(random_nums(256));
    $r_power = json_encode(random_nums(256));



    if(empty($lat) || empty($lon)) {
        $_SESSION['danger'][] = 'Both Latitude and Longitude are required';
    }elseif($lat > 90 || $lat < -90 || $lon > 190 || $lon < -180) {
        $_SESSION['danger'][] = 'Coordinates outside of range longitude (-90,90) or latitude (-180,180)';
    }elseif(strlen($intent) > 64) {
        $_SESSION['danger'][] = 'Intent must be less than 64 characters long';
    }elseif(User::get_plots($account_user_id, ($date-86400)) > 15){
        $_SESSION['danger'][] = 'At this time you are only allowed to create 15 plots every 24 hours';
    }

    /* If there are no errors, add the server to the database */
    if(empty($_SESSION['danger'])) {
        /* Add the server to the database as private */
        $stmt = $database->prepare("INSERT INTO `plots` (`id`, `user_id`, `latitude`, `longitude`, `intent`, `datetime`, `q_latitude`, `q_longitude`, `q_power`, `ip`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ssssssssss', $id, $account_user_id, $lat, $lon, $intent, $date, $r_lat, $r_lon, $r_power, $_SERVER['REMOTE_ADDR']);
        $stmt->execute();
        $stmt->close();

        /* Set the success message and redirect */
        $_SESSION['success'][] = 'New Plot Generated.';
        redirect('/plot/' . $id);
    }

    display_notifications();

}

initiate_html_columns();


?>

<script>
    $(document).ready(function(){
        $("#coord").click(function(){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                console.log("Geolocation is not supported by this browser.");
            }
        });
        var x = document.getElementById("lat");
        var y = document.getElementById("lon");
        function showPosition(position) {
            x.value = Number((position.coords.latitude).toFixed(6));
            y.value = Number((position.coords.longitude).toFixed(6));

        }
    });
</script>

<div class="col-lg-3"></div>
<div class="col-lg-6 boxed">
    <h3 class="title">New Plot</h3>
    <div class="mw-100 p-3 text-center">

    <form action="" method="post" role="form">
            <p class="text-center m-0">Enter a base Coordinate to generate numbers from</p>
            <hr>
            <div class="row">
                <div class="form-group col-12">
                    <span id="coord" role="button" class="badge badge-info"><i class="fas fa-location-arrow"></i> Current Location</span>

                </div>
                <div class="form-group col-6">
                    <label>Latitude*</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-crosshairs"></i></div>
                        </div>
                        <input id="lat" type="text" name="lat" class="form-control" placeholder="00.0000" value="<?php echo $lat; ?>" />
                    </div>
                </div>
                <div class="form-group col-6">
                    <label>Longitude*</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-crosshairs"></i></div>
                        </div>
                        <input id="lon" type="text" name="lon" class="form-control" placeholder="-00.0000" value="<?php echo $lon; ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="far fa-lightbulb"></i></div>
                    </div>
                    <input type="text" name="intent" class="form-control" placeholder="Intent (Optional)" />
                </div>
            </div>
            <hr>

            <div class="form-group text-center">
                <button type="submit" name="submit" class="btn btn-primary col-8">Generate Plot</button>
            </div>

        </form>
    </div>

</div>


