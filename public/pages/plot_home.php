<?php
//TODO: maxpoints per plot
User::check_permission(0);

$pid = filter_var($_GET['plot_id'], FILTER_SANITIZE_STRING);
$type = 0;

if(empty($_GET['plot_id'])) {
    redirect();
}

if(empty($_GET['plot_id']) || !$plot->exists) {
    $_SESSION['danger'][] = 'Sorry, this plot is no longer available.';
    redirect();
} else {
    /* Check if server is disabled */
    if($plot->data->public == 0 && $plot->data->user_id != $account_user_id && User::get_type($account_user_id) != 1) {
        $_SESSION['danger'][] = 'Sorry, this plot is no longer available.';
        redirect();
    }elseif (User::get_type($account_user_id) == 1){
        $type = 1;
    }
}

if(!empty($_POST)) {
    $raw_data = filter_var($_POST['coord'], FILTER_SANITIZE_STRING);
    $response = array();
    $notes = '';
    $public = 0;

    if(empty($raw_data)) {
        $_SESSION['danger'][] = 'A coordinate point must be selected';
    }

    if(empty($_SESSION['danger'])) {
        /* Define some needed variables | set $active=1 for email confirmation*/
        $point_number = substr(md5(uniqid(rand(), true)), -4);
        $plot_id = $plot->data->id;
        $point_id = $plot_id.'-'.$point_number;
        $date = time();
        $latitude = explode(',', explode('_', $raw_data)[0])[0];
        $longitude = explode(',', explode('_', $raw_data)[0])[1];
        $power = explode(',', explode('_', $raw_data)[0])[2];
        $rw = explode('_', $raw_data)[1];
        $response_str = null;


        /* Add the user to the database */
        $stmt = $database->prepare("INSERT INTO `points` (`id`, `point_number`, `latitude`, `longitude`, `plot_id`, `response`, `notes`, `power`, `timestamp`, `raw_data`, `public`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssssssssss', $point_id, $point_number, $latitude, $longitude, $plot_id, $response_str, $notes, $power, $date, $rw, $public);
        $stmt->execute();
        $stmt->close();

        $_SESSION['success'][] = 'Point successfully created';
        redirect('/point/'.$point_id);
    }

    display_notifications();

}

initiate_html_columns();
$p_lat = [];
$p_lon = [];
$p_id = [];
?>

<script>
    var map_string = '';
    var map_extra = '';
    var isMobile = /iPhone|iPad|iPod|Android|IEMobile/i.test(navigator.userAgent);
    if(!isMobile){
        map_string = 'Google';
    }else{
        map_link = '?q=Point_%{text}&ll=';
        map_string = 'Apple';
    }
</script>
<div class="col-lg-2"></div>
<div class="col-lg-8">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/user-plots">Plots</a></li>
            <li class="breadcrumb-item active"><?php
                echo $plot->data->id;
                echo ($type == 1) ? ' - Admin ('.User::x_to_y('user_id', 'username', $plot->data->user_id).')' : '';
                ?></li>
        </ol>
    </nav>
</div>
<div class="col-lg-2"></div>


<script src="https://cdn.plot.ly/plotly-latest.min.js" charset="utf-8"></script>
<div class="col-lg-2"></div>
<div class="col-lg-8 boxed mw-100">
    <h3 class="title">Plot <?php echo $plot->data->id; ?></h3>

    <div class="mw-100 p-3 p-sm-0 text-center">
        <div Class="row">
            <div class="col-12">
                <div id='plot' class="rounded mx-auto img-fluid"></div>
            </div>
        </div>
        <span id="map_info" class="badge badge-info mx-auto mt-2" style="display:none;">Click a point to open in Google maps</span>

        <hr>
        <table class="table table-bordered text-left">
            <tbody>
            <tr>
                <td><strong>Base Latitude</strong></td>
                <td><?php echo $plot->data->latitude; ?></td>
            </tr>
            <tr>
                <td><strong>Base Longitude</strong></td>
                <td><?php echo $plot->data->longitude; ?></td>
            </tr>
            <?php
            if(!empty($plot->data->intent)){
                ?>
            <tr>
                <td><strong>Intent</strong></td>
                <td><?php echo $plot->data->intent; ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td><strong>Date</strong></td>
                <td><?php
                    $userTimezone = new DateTimeZone($plot->user->timezone);
                    $gmtTimezone = new DateTimeZone('GMT');
                    $datestr = strftime('%c', $plot->data->datetime);
                    $myDateTime = new DateTime($datestr, $gmtTimezone);
                    $offset = $userTimezone->getOffset($myDateTime);

                    echo strftime('%b %e, %y (%I:%M %P)', ($plot->data->datetime+$offset)); ?>
                </td>
            </tr>
            </tbody>
            </tbody>
        </table>

        <?php if (User::get_type($account_user_id) == 1){ ?>
            <div class="row">
                <div class="col-12 text-left">
                    <?php
                    $result = $database->query("SELECT * FROM `points` WHERE `plot_id` = '{$pid}' ORDER BY `timestamp` DESC");
                    $count = $result->num_rows;

                    if($count > 0){ ?>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Point #</th>
                                <th scope="col">Coordinates</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while($request = $result->fetch_object()) {
                                $point = new Point($request->id);
                                array_push($p_lat, $point->data->latitude);
                                array_push($p_lon, $point->data->longitude);
                                array_push($p_id, $point->data->point_number);
                                ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $settings->url?>/point/<?php echo $point->data->id; ?>" > <?php echo $point->data->point_number; ?></a>
                                </td>
                                <td>
                                    <?php if($point->data->public == 1){
                                        echo '<i data-toggle="tooltip" data-placement="top" title="Public" class="fas fa-globe-americas"></i> ';
                                    }else{
                                        echo '<i data-toggle="tooltip" data-placement="top" title="Private" class="fas fa-lock"></i> ';

                                    } ?>
                                     (<?php echo $point->data->latitude; ?>, <?php echo $point->data->longitude; ?>) <?php echo $point->data->power; ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>



</div>

<script>
    var q_data = {
        "lat": {
            "base":<?php echo $plot->data->latitude; ?>,
            "map": []
        },
        "lon": {
            "base": <?php echo $plot->data->longitude; ?>,
            "map": []
        },
        "stg": {
            "z": [],
            "r": [],
            "pct": []
        },
        "point_data": {
            "base": Array.from(Array(256).keys()),
            "map": [],
        }
    }

    var q_lat = <?php echo $plot->data->q_latitude; ?>;
    var q_lon = <?php echo $plot->data->q_longitude; ?>;
    var q_strength = <?php echo $plot->data->q_power; ?>;


    for(var i=0; i< q_lat.length; i++) {
        var n0 = (((q_lat[i]-32768))/10000000);
        var n1 = (((q_lon[i]-32768))/10000000);
        var n2 = ((10)*((q_strength[i])/(65535)));
        console.log(n2);

        q_data['lat']['map'].push(Number((q_data['lat']['base'] + n0).toFixed(6)));
        q_data['lon']['map'].push(Number((q_data['lon']['base'] + n1).toFixed(6)));
        q_data['stg']['z'].push(Number((n2).toFixed(6)));
        q_data['stg']['r'].push(Number((20+2*n2).toFixed(6)));
        q_data['stg']['pct'].push(Number((((q_strength[i])/(65535))*100).toFixed(2)));

        q_data['point_data']['map'].push([i, q_lat[i], q_lon[i], q_strength[i]]);
    }
    console.log(q_data['stg']['z']);
    console.log(q_data['stg']['r']);


    var quantum_field = {
        name: 'Quantum Field',
        type: "densitymapbox",
        lat: q_data['lat']['map'],
        lon: q_data['lon']['map'],
        z: q_data['stg']['z'],
        radius: q_data['stg']['r'],
        showscale: false,
        colorscale: "Viridis",
        opacity: 0.7,
        hoverinfo: "none"
    }

    var point_trace = {
        name: 'Quantum Point',
        type: "scattermapbox",
        lat: q_data['lat']['map'],
        lon: q_data['lon']['map'],
        text: q_data['point_data']['base'],
        customdata: q_data['stg']['pct'],
        showlegend: false,
        opacity: 0.6,
        marker: {
            size: 8,
            color: q_data['point_data']['base'],
            colorscale: "YlOrRd"
        },
        hovertemplate: 'Point #%{text}<br>Intensity %{customdata}%<br>(%{lat:.6f},%{lon:.6f})<br>' +
            '<a target="_blank" href="http://maps.'+map_string+'.com/?q='+map_extra+'%{lat:.6f},%{lon:.6f}">'+map_string+' Maps </a><extra></extra>'
    }

    var data = [quantum_field, point_trace];


    var layout = {
        dragmode: "zoom",
        mapbox: {
            style: "open-street-map",
            center: { lat: q_data['lat']['base'], lon: q_data['lon']['base'] },
            zoom: 15.5
        },
        coloraxis: {
            showscale: false,
            colorscale: "Viridis"
        },
        margin: { r: 0, t: 0, b: 0, l: 0 }
    };

    var config = {
        responsive: true,
        displayModeBar: false,
        displaylogo: false,
        logging: 0
    };

    Plotly.newPlot("plot", data, layout, config);

    $( document ).ready(function() {
        if(!isMobile) {
            plot.on('plotly_click', function (data) {
                for (var i = 0; i < data.points.length; i++) {
                    var id = data.points[i].pointIndex;
                    window.open("http://maps.google.com/?q=" + q_data['lat']['map'][id] + "," + q_data['lon']['map'][id]);
                }
            });
            $('#map_info').show();
        }else{
            $('#map_info').text("Click a point to view point properties");
            $('#map_info').show();
        }
    });

</script>