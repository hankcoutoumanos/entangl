<?php
//TODO: Status
User::check_permission(0);

$pid = filter_var($_GET['plot_id'], FILTER_SANITIZE_STRING);

if(empty($_GET['plot_id'])) {
    redirect();
}

if(empty($_GET['plot_id']) || !$plot->exists) {
    $_SESSION['danger'][] = 'Sorry, this map is no longer available.';
    redirect();
} else {
    $lats = [];
    $lons = [];
    $pows = [];
    $result = $database->query("SELECT * FROM `points` WHERE `plot_id` = '{$pid}' ORDER BY `timestamp` DESC");
    $count = $result->num_rows;
    if($count > 0){
        $_SESSION['danger'][] = 'Sorry, this map has no points shared.';
        redirect();
    }

    while ($request = $result->fetch_object()) {
        array_push($lats, $request->latitude);
        array_push($lons, $request->longitude);
        array_push($pows, $request->power);
    }
    $lat_c = max($lats)-((max($lats)-min($lats))/2);
    $lon_c = max($lons)-((max($lons)-min($lons))/2);

}

if(!empty($_POST)) {
    $raw_data = filter_var($_POST['coord'], FILTER_SANITIZE_STRING);
    $response = array();
    $notes = filter_var($_POST['notes'], FILTER_SANITIZE_STRING);

    if(!isset($_POST['response'])){
        $response = '';
    }else{
        foreach($_POST['response'] as $option){
            array_push($response, filter_var($option, FILTER_SANITIZE_NUMBER_INT));
        }
    }

    if(isset($_POST['public'])){
        $public = filter_var($_POST['public'], FILTER_SANITIZE_NUMBER_INT);
    }else{
        $public = 0;
    }


    if(strlen($notes) > 2048) {
        $_SESSION['danger'][] = 'Sorry, but the notes must be less than 2048 characters in length';
    }elseif(empty($raw_data)) {
        $_SESSION['danger'][] = 'A coordinate point must be selected';
    }

    if(empty($_SESSION['danger'])) {
        /* Define some needed variables | set $active=1 for email confirmation*/
        $point_id = substr(md5(uniqid(rand(), true)), -12);
        $plot_id = $plot->data->id;
        $date = time();
        $latitude = explode(',', explode('_', $raw_data)[0])[0];
        $longitude = explode(',', explode('_', $raw_data)[0])[1];
        $power = explode(',', explode('_', $raw_data)[0])[2];
        $rw = explode('_', $raw_data)[1];
        $response_str = implode(',', $response);


        /* Add the user to the database */
        $stmt = $database->prepare("INSERT INTO `points` (`id`, `latitude`, `longitude`, `plot_id`, `response`, `notes`, `power`, `timestamp`, `raw_data`, `public`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ssssssssss', $point_id, $latitude, $longitude, $plot_id, $response_str, $notes, $power, $date, $rw, $public);
        $stmt->execute();
        $stmt->close();

        $_SESSION['success'][] = 'Point successfully created';
        redirect('/plot/'.$plot_id);
    }

    display_notifications();

}

initiate_html_columns();
?>

<script src="https://cdn.plot.ly/plotly-latest.min.js" charset="utf-8"></script>

<div class="col-lg-2"></div>
<div class="col-lg-8 boxed mw-100">
    <h3 class="title">Plot <?php echo $plot->data->id; ?></h3>

    <div class="mw-100 p-3 text-center">
        <div Class="row">
            <div class="col-12">
                <div id='plot' class="rounded mx-auto img-fluid"></div>
            </div>
        </div>

        <hr>
        <table class="table table-bordered text-left">
            <tbody>
            <tr>
                <td><strong>Base Latitude</strong></td>
                <td><?php echo $plot->data->latitude; ?></td>
            </tr>
            <tr>
                <td><strong>Base Longitude</strong></td>
                <td><?php echo $plot->data->longitude; ?></td>
            </tr>
            <?php
            if(!empty($plot->data->intent)){
                ?>
                <tr>
                    <td><strong>Intent</strong></td>
                    <td><?php echo $plot->data->intent; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td><strong>Date</strong></td>
                <td><?php
                    $userTimezone = new DateTimeZone($plot->user->timezone);
                    $gmtTimezone = new DateTimeZone('GMT');
                    $datestr = strftime('%c', $plot->data->datetime);
                    $myDateTime = new DateTime($datestr, $gmtTimezone);
                    $offset = $userTimezone->getOffset($myDateTime);

                    echo strftime('%b %e, %y (%I:%M %P)', ($plot->data->datetime+$offset)); ?>
                </td>
            </tr>
            </tbody>
            </tbody>
        </table>
    </div>



</div>

<script>
    var i_r_lat = <?php echo $lat_c; ?>;
    var i_r_lon = <?php echo $lon_c; ?>;

    var r_lat = <?php echo $lats; ?>;
    var r_lon = <?php echo $lons; ?>;
    var power = <?php echo $pows; ?>;
    var i = 0;
    var len = i_r_lat.length;
    var complete = [];

    var trace = {
        type: "densitymapbox",
        lat: r_lat,
        lon: r_lon,
        z: power,
        radius: 35,
        coloraxis: 'coloraxis',
        opacity: 0.7
    }
    var data = [trace];

    var layout = {
        dragmode: "zoom",
        mapbox: {
            style: "open-street-map",
            center: { lat: i_lat, lon: i_lon },
            zoom: 15
        },
        coloraxis: {
            showscale: false,
            colorscale: "Viridis"
        },
        margin: { r: 0, t: 0, b: 0, l: 0 }
    };

    var config = {
            responsive: true,
            displayModeBar: false,
            displaylogo: false,
            logging: 0
        }
    ;

    Plotly.newPlot("plot", data, layout, config);

</script>



