<?php
User::logged_in_redirect();
include 'core/functions/recaptchalib.php';

$id = substr(md5(uniqid(rand(), true)), -12);
$name = '';
$email = '';


if(!empty($_POST)) {
    /* Clean some posted variables */
    $name = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
    $tz = filter_var($_POST['timezone'], FILTER_SANITIZE_NUMBER_FLOAT);

    $captcha = recaptcha_check_answer($settings->captcha_private, $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);

    /* Define some variables */
    $fields = array('username', 'password', 'repeat_password', 'g-recaptcha-response');

    /* Check for any errors */
    foreach($_POST as $key=>$value) {
        if(empty($value) && in_array($key, $fields) == true) {
            $_SESSION['danger'][] = 'Please fill in all of the fields!';
            break 1;
        }
    }

    if(empty($_SESSION['danger'])) {
        if(!$captcha->is_valid) {
            $_SESSION['danger'][] = 'Invalid Captcha!';
        }
        if(User::x_exists('username', $name)) {
            $_SESSION['danger'][] = 'Sorry, but the name <strong>' . $_POST['username'] . '</strong> is taken!';
        }
        if(strlen(trim($_POST['password'])) < 6) {
            $_SESSION['danger'][] = 'Your password must be longer than 6 characters!';
        }
        if($_POST['password'] !== $_POST['repeat_password']) {
            $_SESSION['danger'][] = 'Passwords do not match!';
        }
    }


    /* If there are no errors continue the registering process */
    if(empty($_SESSION['danger'])) {
        /* Define some needed variables | set $active=1 for email confirmation*/
        $password 	= User::encrypt_password($name, $_POST['password']);
        $active 	= 0;
        $email_code = md5($name.microtime());
        $date = time();

        /* Add the user to the database */
        $stmt = $database->prepare("INSERT INTO `users` (`unique_id`, `username`, `password`, `email`, `email_activation_code`, `active`, `ip`, `date`, `timezone`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssssssss', $id, $name, $password, $email, $email_code, $active, $_SERVER['REMOTE_ADDR'], $date, $tz);
        $stmt->execute();
        $stmt->close();

        $_SESSION['success'][] = 'Account successfully created you may now log in';
        redirect('/login');

    }

    display_notifications();

}

initiate_html_columns();

?>

<div class="col-md-2 col-lg-3"></div>
<div class="col-md-8 col-lg-6 boxed">
    <h3 class="title">New Account</h3>

    <form action="" method="post" role="form">
        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                </div>
                <input type="text" value="<?php echo $name; ?>" name="username" class="form-control" placeholder="Username"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                </div>
                <input type="email" value="<?php echo $email; ?>" name="email" class="form-control" placeholder="Email (Optional)"/>
            </div>
        </div>

        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-lock"></i></div>
                </div>
                <input type="password" name="password" class="form-control" placeholder="Password" />
            </div>
        </div>

        <div class="form-group">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-redo"></i></div>
                </div>
                <input type="password" name="repeat_password" class="form-control" placeholder="Confirm Password" />
                <input id="tz" type="hidden" name="timezone" value="">
            </div>
        </div>

        <div class="form-group d-flex justify-content-center">
            <?php echo recaptcha_get_html($settings->captcha_public); ?>
        </div>

        <div class="form-group text-center">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Create Account</button>
        </div>



    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var visitortime = new Date();
        var visitortimezone = -visitortime.getTimezoneOffset()/60;
        var tz = visitortimezone.toString().replace(/-/g, "");
        var output = '';
        if(tz.length > 1){
            if(visitortimezone > 0) {
                output = '+' + tz + '00';
            }else {
                output = '-' + tz + '00';
            }
        }else{
            if(visitortimezone > 0) {
                output = '+0' + tz + '00';
            }else {
                output = '-0' + tz + '00';
            }
        }
        $('#tz').val(output);
    });
</script>

