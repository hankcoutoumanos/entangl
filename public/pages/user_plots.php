<?php
//TODO: Pagination, Filter, Sort, Timezone user db
User::check_permission(0);

display_notifications();

initiate_html_columns();

?>


<div class="col-lg-2 col-md-1"></div>
<div class="col-lg-8 col-md-10 col-sm-12 boxed">
    <h3 class="title">My Plots</h3>

    <div class="col-12 pt-3">
        <?php
        $total_items = $database->query("SELECT `id` FROM `plots` WHERE `user_id` = {$account_user_id}");

        if($total_items->num_rows < 1){
            echo output_notice('You have not created any plots yet!').'<hr>';
        }else{
            $pages = new Pagination(10, $total_items->num_rows);
            $result = $database->query("SELECT * FROM `plots` WHERE `user_id` = {$account_user_id} ORDER BY `datetime` DESC {$pages->limit}");
        ?>

            <div class="table-responsive">
                <table class="table" style="min-width:500px;">
                    <thead>
                    <tr>
                        <th>Plot ID (Intent)</th>
                        <th>Coordinates</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>

                <?php while($request = $result->fetch_object()) {
                    $plot = new Plot($request->id);

                    ?>
                        <tr>
                            <td>
                                <a href="<?php echo $settings->url?>/plot/<?php echo $plot->data->id; ?>" ><?php echo $plot->data->id; ?></a><br>
                                <?php if(!empty($plot->data->intent)) echo $plot->data->intent; ?>

                            </td>
                            <td>
                                (<?php echo $plot->data->latitude;?>, <?php echo $plot->data->longitude;?>)

                            </td>
                            <td>
                                <?php
                                $userTimezone = new DateTimeZone($account->timezone);
                                $gmtTimezone = new DateTimeZone('GMT');
                                $datestr = strftime('%c', $plot->data->datetime);
                                $myDateTime = new DateTime($datestr, $gmtTimezone);
                                $offset = $userTimezone->getOffset($myDateTime);

                                echo strftime('%b %e, %y %l:%M %P', ($plot->data->datetime+$offset));

                                ?>
                            </td>
                        </tr>


                <?php } ?>
                    </tbody>
                </table>
                <?php if($total_items > 0){
                    $pages->set_current_page_link('user-plots');
                    $pages->display();
                } ?>
            </div>
        <?php } ?>


    </div>

</div>


