<?php
#TODO:

initiate_html_columns();

?>


<div class="col-md-1 col-lg-2"></div>
<div class="col-md-10 col-lg-8 boxed">
    <h3 class="title">Entangl App Info</h3>
    <br>

    <div class="col-12">
    <h4>How does entangl work?</h4>
    <ul>
        <li>Entangl uses quantum generated numbers to create a point</li>
        <li>A set of many random points are then plotted together</li>
        <li>Users then travel to these generated points/areas</li>
    </ul><hr>
    <h4>How is a map made?</h4>
    <ul>
        <li>Each point is made of 3 randomly generated numbers and their index</li>
        <li>(index, latitude, longitude, strength)</li>
        <li>These are plotted according to its position and strength</li>
        <li>Using an intent can help generate meaningful points</li>
    </ul><hr>

    <h4>What is the map colorscale for?</h4>
    <ul>
        <li>Colors range from purple to yellow representing a points intensity</li>
        <li>Strength is a reflection of the positions "quantum importance"</li>
        <li>Higher strengths are marked by brighter yellow colors</li>
        <li>The correlation between point strength and point meaning varies between users</li>
        <li>However after traveling to several different points a pattern can be deciphered</li>
    </ul><hr>

    <h4>Which points do I go to?</h4>
    <ul>
        <li>There is no "right" point to go to</li>
        <li>Some users find that almost every point and location has a story</li>
        <li>Simply focus on your intent, have an open mind and be observant</li>
    </ul><hr>

    <h4>How can I start?</h4>
    <ul>
        <li>Create a <a href="<?php echo $settings->url; ?>/register">new account</a> and then <a href="<?php echo $settings->url; ?>/login">login</a></li>
        <li>Generate a new plot</li>
        <li>Select a single point on the plot by clicking a point and selecting activate</li>
        <li>Open newly created point can be opened using Apple or Google maps</li>
        <li>Travel to the point of interest and share any cool findings with the entangl community</li>
    </ul><hr>

    </div>


</div>


