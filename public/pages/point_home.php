<?php
//TODO: Status
User::check_permission(0);
$type = 0;
$pid = filter_var($_GET['point_id'], FILTER_SANITIZE_STRING);
$notes = '';
if(empty($_GET['point_id'])) {
    redirect();
}

if(empty($_GET['point_id']) || !$point->exists) {
    $_SESSION['danger'][] = 'Sorry, this point is no longer available.';
    redirect();
} else {
    /* Check if server is disabled */
    if($point->data->public == 0 && $point->plot->user_id != $account_user_id && User::get_type($account_user_id) != 1) {
        $_SESSION['danger'][] = 'Sorry, this plot is no longer available.';
        redirect();
    }elseif (User::get_type($account_user_id) == 1){
        $type = 1;
    }
}

if(!empty($_POST)) {
    $notes = filter_var($_POST['notes'], FILTER_SANITIZE_STRING);
    if(isset($_POST['public'])){
        $public = filter_var($_POST['public'], FILTER_SANITIZE_NUMBER_INT);
    }else{
        $public = 0;
    }

    if(isset($_POST['verify'])){
        $verify = filter_var($_POST['verify'], FILTER_SANITIZE_NUMBER_INT);
    }else{
        $verify = 0;
    }


    if(strlen($notes) > 2048) {
        $_SESSION['danger'][] = 'Sorry, but the notes must be less than 2048 characters in length';
    }elseif($account_user_id != $point->plot->user_id){
        $_SESSION['danger'][] = 'Sorry, it appears an error occurred';
    }elseif($verify == 1){
        $verfied = 0;
        $current_lat = filter_var($_POST['lat'], FILTER_SANITIZE_STRING);
        $current_lon = filter_var($_POST['lon'], FILTER_SANITIZE_STRING);
        if(empty($current_lat) || empty($current_lon)){
            $_SESSION['danger'][] = 'Sorry, it appears an error occurred';
        }else{
            $diff_lat = abs($point->data->latitude - $current_lat);
            $diff_lon = abs($point->data->longitude - $current_lon);
            if($diff_lat > 0.00025 || $diff_lon > 0.00025){
                $_SESSION['danger'][] = 'Sorry, it appears you are too far away from the point to verify';
            }else{
                $verfied = 1;
            }
        }

    }

    if(empty($_SESSION['danger'])) {
        if(empty($notes) && ($public != $point->data->public || $verfied != $point->data->verified)){
            $public = ($public != $point->data->public)? $public : $point->data->public;
            $verfied = ($verfied != $point->data->verified)? $verfied : $point->data->verified;

            $stmt = $database->prepare("UPDATE `points` SET `public` = ?, `verified` = ? WHERE `id` = '{$point->data->id}'");
            $stmt->bind_param('ss', $public, $verfied);
            $stmt->execute();
            $stmt->close();

        }elseif(!empty($notes)){
            $public = ($public != $point->data->public)? $public : $point->data->public;
            if($verify == 1){
                $stmt = $database->prepare("UPDATE `points` SET `public` = ?, `verified` = ?, `notes` = ? WHERE `id` = '{$point->data->id}'");
                $stmt->bind_param('sss', $public, $verfied, $notes);
                $stmt->execute();
                $stmt->close();
            }else{
                $stmt = $database->prepare("UPDATE `points` SET `public` = ?, `notes` = ? WHERE `id` = '{$point->data->id}'");
                $stmt->bind_param('ss', $public, $notes);
                $stmt->execute();
                $stmt->close();
            }
        }

        $_SESSION['success'][] = 'Point updated';
        redirect('/point/'.$pid);
    }

    display_notifications();

}


initiate_html_columns();
?>

<?php
if(User::logged_in() == true && ($account_user_id == $point->plot->user_id || $type == 1)){ ?>
<div class="col-lg-2"></div>
<div class="col-lg-8">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/user-plots">Plots</a></li>
            <li class="breadcrumb-item"><a href="/plot/<?php echo $point->data->plot_id; ?>">
                    <?php
                    echo $point->data->plot_id;
                    ?>
                </a></li>
            <li class="breadcrumb-item active"><?php
                echo $point->data->point_number;
                echo ($type == 1) ? ' - Admin ('.User::x_to_y('user_id', 'username', $point->plot->user_id).')' : '';
                ?></li>
        </ol>
    </nav>
</div>
<div class="col-lg-2"></div>
<?php } ?>

<script src="https://cdn.plot.ly/plotly-latest.min.js" charset="utf-8"></script>

<div class="col-lg-2"></div>
<div class="col-lg-8 boxed mw-100">
    <h3 class="title">Point <?php echo $point->data->point_number; ?></h3>

    <div class="mw-100 p-3">
        <div class="row mb-2">
            <ul class="nav nav-tabs col-12">
                <li class="nav-item">
                    <a class="nav-link hoverable active" id="map">Map</a>
                </li>
                <?php if($point->plot->user_id === $account_user_id){ ?>
                <li class="nav-item">
                    <a class="nav-link hoverable" id="att">Properties</a>
                </li>
                <?php } ?>
            </ul>
        </div>

        <div Class="row">
            <div id="map_c" class="col-12">
                <div id='plot' class="rounded mx-auto img-fluid"></div>
                <div class="col-12 pt-2 text-center" id="ios" style="display:none">
                    <a target="_blank" class="btn btn-danger" href="http://maps.apple.com/?q=<?php echo $point->plot->intent.'&ll='.$point->data->latitude.','.$point->data->longitude; ?>" role="button">Open in Apple Maps <i class="fas fa-external-link-alt"></i></a>
                </div>
                <div class="col-12 pt-2 text-center" id="goo" style="display:none">
                    <a target="_blank" class="btn btn-danger" href="http://maps.google.com/?q=<?php echo $point->data->latitude.','.$point->data->longitude; ?>" role="button">Open in Google Maps <i class="fas fa-external-link-alt"></i></a>
                </div>
            </div>
            <?php if($point->plot->user_id === $account_user_id){ ?>
            <div id="att_c" class="col-12" style="display:none;min-height:450px;">
                <form id="update" action="" method="post" role="form">
                    <?php if(empty($point->data->notes)){ ?>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Notes <small class="text-muted">observations or coincides from a trip to the point</small></label>
                            <textarea id="notes" name="notes" class="form-control" rows="3" placeholder="Optional"><?php echo $notes; ?></textarea>
                        </div>
                    </div>
                    <?php }
                    if($point->data->verified != 1){ ?>
                        <div class="form-group">
                        <div class="form-check col-md-12">
                            <input id="verify" name="verify" class="form-check-input" type="checkbox" value="1">
                            <label class="form-check-label">Verify Location <small class="text-muted">verifies you are near point and adds check to notes</small></label>
                            <input id="lat" type="hidden" name="lat" />
                            <input id="lon" type="hidden" name="lon" />
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="form-check col-md-12">
                            <input name="public" class="form-check-input" type="checkbox" value="1" <?php if($point->data->public == 1){ echo 'checked'; } ?>>
                            <label class="form-check-label">Public <small class="text-muted">coordinates, intent, notes, and plot id for this point only will be public</small></label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group">
                            <button id="veri" type="button" class="btn btn-success">Update Point</button>
                        </div>
                    </div>
                </form>
            </div>
            <?php } ?>
        </div>

        <hr>
        <table class="table table-bordered text-left">
            <tbody>
            <?php
            if(!empty($point->plot->intent)){
                ?>
                <tr>
                    <td><strong>Intent</strong></td>
                    <td><?php echo $point->plot->intent; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td><strong>Quantum Data</strong></td>
                <td><?php echo str_replace(",", " ", $point->data->raw_data); ?></td>
            </tr>
            <tr>
                <td><strong>Plot ID</strong></td>
                <td><a href="<?php echo $settings->url.'/plot/'.$point->data->plot_id; ?>"><?php echo $point->data->plot_id; ?></a></td>
            </tr>
            <tr>
                <td><strong>Plot Date</strong></td>
                <td><?php
                    if(empty($point->user->timezone)){
                        $tz = '+0000';
                    }else{
                        $tz = $point->user->timezone;
                    }
                    $userTimezone = new DateTimeZone($tz);
                    $gmtTimezone = new DateTimeZone('GMT');
                    $datestr = strftime('%c', $point->plot->datetime);
                    $myDateTime = new DateTime($datestr, $gmtTimezone);
                    $offset = $userTimezone->getOffset($myDateTime);

                    echo strftime('%b %e, %y %l:%M %P', ($point->plot->datetime+$offset)); ?>
                </td>
            </tr>
            </tbody>
            </tbody>
        </table>

        <div Class="row">
            <div class="col-12 text-left">
                <h3 class="text-left notes">Notes <?php if($point->data->verified == 1){ echo '<span class="badge badge-info"><i class="fas fa-map-marked-alt"></i> Verified</span>'; }?></h3>
                <hr>
                <?php if($point->data->notes != ''){ ?>
                    <p class="lead text-left mb-1.5"><?php echo $point->data->notes; ?></p>
                <?php } ?>

            </div>
        </div>

    </div>
</div>


<script>
    var point_lat = <?php echo $point->data->latitude; ?>;
    var point_lon = <?php echo $point->data->longitude; ?>;
    var point_power = <?php echo $point->data->power; ?>;

    var trace = {
        type: "densitymapbox",
        lat: [point_lat],
        lon: [point_lon],
        z: [point_power],
        text: [point_power],
        radius: 35,
        coloraxis: 'coloraxis',
        opacity: 0.7,
        hovertemplate: '(%{lat},%{lon}) %{text}<extra></extra>'
    }
    var data = [trace];

    var layout = {
        dragmode: "zoom",
        mapbox: {
            style: "open-street-map",
            center: { lat: point_lat, lon: point_lon },
            zoom: 15
        },
        coloraxis: {
            showscale: false,
            colorscale: "Viridis"
        },
        margin: { r: 0, t: 0, b: 0, l: 0 }
    };

    var config = {
            responsive: true,
            displayModeBar: false,
            displaylogo: false,
            logging: 0
        }
    ;

    Plotly.newPlot("plot", data, layout, config);

    $("#map").click(function() {
        $("#map_c").show();
        $("#att_c").hide();
        $("#map").addClass("active");
        $("#att").removeClass("active");
    });
    $("#att").click(function(){
        $("#map_c").hide();
        $("#att_c").show();
        $("#map").removeClass("active");
        $("#att").addClass("active");
    });

    var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if(isIOS){
        $("#ios").show();
    }else{
        $("#goo").show();
    }

</script>

<script>
    $(document).ready(function(){
        var vv = document.getElementById("verify");
        $("#veri").click(function(){
            if(typeof(vv) != 'undefined' && vv != null) {
                if(document.getElementById("verify").checked) {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition);
                    } else {
                        vv.value = 0;
                        document.getElementById("update").submit();
                    }
                }else{
                    document.getElementById("update").submit();
                }
            }else{
                document.getElementById("update").submit();
            }
        });

        function showPosition(position) {
            var x = document.getElementById("lat");
            var y = document.getElementById("lon");
            x.value = Number((position.coords.latitude).toFixed(6));
            y.value = Number((position.coords.longitude).toFixed(6));
            document.getElementById("update").submit();
        }
    });
</script>



