
<nav class="navbar navbar-light bg-light footer">
    <span class="navbar-text"><?php echo 'Copyright &copy; ' . date("Y") . ' ' . $settings->title; ?></span>
    <div>
        <a href="<?php echo $settings->url; ?>/contact">Contact</a>
    </div>
</nav>
