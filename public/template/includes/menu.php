

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?php echo $settings->url; ?>"><i class="fas fa-dice-d20"></i> entangl</a>

        <?php if(User::logged_in()) { ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/new-plot">New Plot</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/user-plots">My Plots</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/logout">Logout</a>
                    </li>
                </ul>
            </div>

        <?php }else{ ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $settings->url; ?>/register">Sign Up</a>
                    </li>
                </ul>
            </div>
        <?php } ?>

    </div>
</nav>

