<head>
    <title><?php echo $page_title; ?></title>
    <base href="<?php echo $settings->url; ?>">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Explore the framework of reality through quantum generated random numbers.">
    <meta name="apple-mobile-web-app-title" content="Entangl">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="msvalidate.01" content="B570A578ED4A2B103B3406C4E2DC7E6B" />

    <meta name="miamor" content="bubs">

    <link rel="canonical" href="<?php echo $settings->url; ?>"/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="template/webfonts/font-fileuploader.css" rel="stylesheet">
    <link href="template/css/custom.css" rel="stylesheet">
    <link href="template/css/fileuploader.min.css" rel="stylesheet">
    <link href="template/css/all.min.css" rel="stylesheet">

    <link rel="icon" type="image/png" sizes="48x48" href="template/images/favicon-2.png">
    <link rel="apple-touch-icon" href="template/images/apple-icon.png">
    <link rel="manifest" href="template/js/manifest.json" >


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="<?php echo $settings->url; ?>/template/js/fileuploader.min.js" type="text/javascript"></script>
    <script src="<?php echo $settings->url; ?>/template/js/particles.min.js" type="text/javascript"></script>
    <script data-ad-client="ca-pub-7688870831060695" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-LG6K0GM5ZK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-LG6K0GM5ZK');
    </script>


    <script> jQuery(function ($) { $("[rel=tooltip]").tooltip() });</script>
    <script type="text/javascript">$(function () {
            $('[data-toggle="tooltip"]').tooltip()})
    </script>

</head>
