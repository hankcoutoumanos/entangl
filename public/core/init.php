<?php
ob_start();
session_start();

include 'database/connect.php';
include 'functions/general.php';
include 'classes/Pagination.php';
include 'classes/User.php';
include 'classes/Plot.php';
include 'classes/Point.php';
include 'classes/Csrf.php';

/* Initialize variables */
$errors 	= array();
$settings 	= settings_data();
$token 		= new CsrfProtection();

/* Set the default timezone if its not set in the ini file */
if (!date_default_timezone_get())
	date_default_timezone_set('America/Chicago');

/* If user is logged in get his data */
if(User::logged_in()) {
	$account_user_id = (isset($_SESSION['user_id']) == true) ? $_SESSION['user_id'] : $_COOKIE['user_id'];
	$account = new User($account_user_id);

	/* Update last activity */
	$database->query("UPDATE `users` SET `last_activity` = unix_timestamp() WHERE `user_id` = {$account_user_id}");
}


if(!empty($_GET['plot_id']) && ($_GET['page'] == 'plot_home' || $_GET['page'] == 'map_home')) {
	$pid = filter_var($_GET['plot_id'], FILTER_SANITIZE_STRING);
	$plot = new Plot($pid);
	if($plot->exists) $_SESSION['plot_id'] = $plot->data->id;
}

if(!empty($_GET['point_id']) && $_GET['page'] == 'point_home') {
	$pid = filter_var($_GET['point_id'], FILTER_SANITIZE_STRING);
	$point = new Point($pid);
	if($point->exists) $_SESSION['point_id'] = $point->data->id;
}

/* Get profile data if needed */
if(!empty($_GET['username']) && $_GET['page'] == 'profile') {
	/* Fetch the users data & Set a session with the profile id for the form */
	$_SESSION['profile_user_id'] = $profile_user_id = User::x_to_y('username', 'user_id', $_GET['username']);

	/* Check if user exists */
	$user_exists = ($profile_user_id !== NULL);

	/* If user exists -> get his profile data */
	if($user_exists) {
		$profile_account = new User($profile_user_id);
	}

}


include 'functions/titles.php';

?>
