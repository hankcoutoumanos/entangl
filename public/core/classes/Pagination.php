<?php

class Pagination {
    public $per_page;
    public $current_page;
    public $total_pages;
    public $limit;
    public $current_page_link;
    public $link;

    public function __construct($per_page, $total_items){
        global $database;
        global $account_user_id;

        $this->per_page = $per_page;
        $this->total_pages = ceil($total_items/$this->per_page);
        $this->current_page = (isset($_GET['current_page'])) ? (int)$_GET['current_page'] : 1;
        $this->limit = "LIMIT ".($this->current_page-1)*$this->per_page.",".$this->per_page;

    }

    public function set_current_page_link($current_page_link) {
        $this->current_page_link = $current_page_link;
        $this->link = $this->current_page_link.'/'.$this->current_page;
    }

    public function display($affix='/', $stages=3, $limit=25, $links=2){
        $last = $this->total_pages;
        $start = (($this->current_page - $links) > 0) ? $this->current_page-$links : 1;
        $end = (($this->current_page + $links) < $last) ? $this->current_page+$links : $last;
        $previous = $this->current_page - 1;
        $next = $this->current_page + 1;

        $pagination = '<hr><nav aria-label="..."><ul class="pagination justify-content-center">';

        $class = ($this->current_page == 1) ? "disabled" : "";
        $pagination .= '<li class="page-item '.$class.'"><a class="page-link" href="'.$this->current_page_link.'/'.$previous.'">&laquo;</a></li>';

        if($start > 1){
            $pagination .= '<li class="page-item"><a class="page-link" href="'.$this->current_page_link.'">1</a></li>';
            $pagination .= '<li class="page-item text-muted"><a class="page-link">1</a></li>';
        }

        for($i = $start; $i <= $end; $i++){
            $class = ($this->current_page == $i) ? "active" : "";
            $pagination .= '<li class="page-item '.$class.'"><a class="page-link" href="'.$this->current_page_link.'/'.$i.'">'.$i.'</a></li>';
        }

        if($end < $last){
            $pagination .= '<li class="page-item text-muted"><a class="page-link">...</a></li>';
            $pagination .= '<li class="page-item"><a class="page-link" href="'.$this->current_page_link.'/'.$last.'">'.$last.'</a></li>';
        }
        $class = ($this->current_page == $last) ? "disabled" : "";
        $pagination .= '<li class="page-item '.$class.'"><a class="page-link" href="'.$this->current_page_link.'/'.$next.'">&raquo;</a></li>';

        $pagination .= '</ul></nav>';

        echo $pagination;

    }



}