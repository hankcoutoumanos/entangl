<?php

class Plot {
    public $exists;
    public $data;

    public function __construct($plot_id) {
        global $database;

        /* Get all the product information from the database */
        $stmt = $database->prepare("SELECT * FROM `plots` WHERE `id` = ?");
        $stmt->bind_param('s', $plot_id);
        $stmt->execute();
        bind_object($stmt, $this->data);
        $stmt->fetch();
        $stmt->close();

        /* Determine if the server exists */
        $this->exists = ($this->data !== NULL) ? true : false;

        if($this->exists) {

            $this->user = new StdClass;

            $stmt = $database->prepare("SELECT `username`, `unique_id`, `timezone` FROM `users` WHERE `user_id` = ?");
            $stmt->bind_param('s', $this->data->user_id);
            $stmt->execute();
            bind_object($stmt, $this->user);
            $stmt->fetch();
            $stmt->close();

        }

    }

    public static function delete_product($plot_id) {
        global $database;
        /* We need to make sure to delete all the data of the specific server */
        $database->query("DELETE FROM `plots` WHERE `id` = {$plot_id}");

    }

}



?>