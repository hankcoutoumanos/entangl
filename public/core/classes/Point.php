<?php

class Point {
    public $exists;
    public $data;

    public function __construct($point_id) {
        global $database;

        /* Get all the product information from the database */
        $stmt = $database->prepare("SELECT * FROM `points` WHERE `id` = ?");
        $stmt->bind_param('s', $point_id);
        $stmt->execute();
        bind_object($stmt, $this->data);
        $stmt->fetch();
        $stmt->close();

        /* Determine if the server exists */
        $this->exists = ($this->data !== NULL) ? true : false;

        if($this->exists){
            $this->plot = new StdClass;

            $stmt = $database->prepare("SELECT `user_id`, `intent`, `datetime` FROM `plots` WHERE `id` = ?");
            $stmt->bind_param('s', $this->data->plot_id);
            $stmt->execute();
            bind_object($stmt, $this->plot);
            $stmt->fetch();
            $stmt->close();

            $this->user = new StdClass;

            $stmt = $database->prepare("SELECT `timezone` FROM `users` WHERE `user_id` = ?");
            $stmt->bind_param('s', $this->plot->user_id);
            $stmt->execute();
            bind_object($stmt, $this->user);
            $stmt->fetch();
            $stmt->close();
        }


    }

    public static function delete_point($point_id) {
        global $database;
        /* We need to make sure to delete all the data of the specific server */
        $database->query("DELETE FROM `points` WHERE `id` = '{$point_id}'");

    }

}



?>