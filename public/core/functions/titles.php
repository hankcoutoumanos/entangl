<?php
$page_title = '';

if(isset($_GET['page'])) {
    switch($_GET['page']) {
        case 'login' : $page_title = 'Login |'; break;
        case 'register' : $page_title = 'New Account |'; break;
        case 'home' : $page_title = ''; break;
        case 'my_maps' : $page_title = 'Plots'; break;
        case 'new_map' : $page_title = 'New Plot'; break;
        case 'plot' : $page_title = ($plot->exists) ? 'Plot '. $plot->data->id : 'Unavailable';	break;
    }
}

$page_title .= $settings->title;

?>