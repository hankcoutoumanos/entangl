 <?php

function redirect($new_page = '/') {
    global $settings;
    header('Location: ' . $settings->url . $new_page);
    die();
}

function refresh() {
    header('Location: '.$_SERVER['REQUEST_URI']);
    die();
}

function random_nums($amount=256) {
    $url = 'https://qrng.anu.edu.au/API/jsonI.php?length='. $amount .'&type=uint16';
    $json = json_decode(file_get_contents($url));

    return $json->data;
}

function bind_object($stmt, &$row) {
    $row = new StdClass;
    $md = $stmt->result_metadata();

    $params = array();
    while($field = $md->fetch_field()) {
        $params[] = &$row->{$field->name};
    }

    call_user_func_array(array($stmt, 'bind_result'), $params);

    $stmt->fetch();

    if($row->{key($row)} == '' || $row->{key($row)} == '0') $row = null;
}

function resize($file_name, $path, $width, $height, $center = false) {
    /* Get original image x y*/
    list($w, $h) = getimagesize($file_name);

    /* calculate new image size with ratio */
    $ratio = max($width/$w, $height/$h);
    $h = ceil($height / $ratio);
    $x = ($w - $width / $ratio) / 2;
    $w = ceil($width / $ratio);
    $y = 0;
    if($center) $y = 250 + $h/1.5;

    /* read binary data from image file */
    $imgString = file_get_contents($file_name);

    /* create image from string */
    $image = imagecreatefromstring($imgString);
    $tmp = imagecreatetruecolor($width, $height);
    imagecopyresampled($tmp, $image,
        0, 0,
        $x, $y,
        $width, $height,
        $w, $h);

    /* Save image */
    imagejpeg($tmp, $path, 100);

    return $path;
    /* cleanup memory */
    imagedestroy($image);
    imagedestroy($tmp);
}

/* Function to return all the settings table */
function settings_data() {
    global $database;

    $result = $database->query("SELECT * FROM `settings` WHERE `id` = 1");
    $data   = $result->fetch_object();

    return $data;
}

/* Initiate html columns */
function initiate_html_columns() {
    include 'template/includes/init_main_column.php';
}

/* Custom die function */
function close($message, $error = false) {
    global $languages;
    global $language;
    global $database;
    global $account_user_id;
    global $server;
    global $category_exists;

    echo $message;

    include 'template/overall_footer.php';
    die();
}

function display_notifications() {

    $types = array("danger", "success", "info");
    foreach($types as $type) {
        if(isset($_SESSION[$type]) && !empty($_SESSION[$type])) {
            if(!is_array($_SESSION[$type])) $_SESSION[$type] = array($_SESSION[$type]);

            do {
                echo '
					<div class="alert alert-' . $type .'">
                        ' . implode('<br>', $_SESSION[$type]) . '
					</div>
				';
            } while (0);
            unset($_SESSION[$type]);
        }
    }

}

function output_errors($errors) {
    if(!is_array($errors)) $errors = array($errors);

    do {
        echo '
            <div class="alert alert-danger">
                ' . implode('<br>', $errors) . '
            </div>
        ';
    } while (0);

}

function output_success($messages) {
    if(!is_array($messages)) $messages = array($messages);

    do {
        echo '
            <div class="alert alert-success">
                ' . implode('<br>', $messages) . '
            </div>
        ';
    } while (0);
}

function output_notice($messages) {
    if(!is_array($messages)) $messages = array($messages);

    do {
        echo '
            <div class="alert alert-info">
                ' . implode('<br>', $messages) . '
            </div>
        ';
    } while (0);
}
?>